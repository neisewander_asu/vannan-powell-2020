# Vannan-Powell-2020

Script utilized in R to analyze nanostring ncounter data on miRNA expression following cocaine self-administration and forced abstinence.

This file was developed by Gregory L. Powell, Ph.D., at Arizona State University (gpowell6@asu.edu). 
Analysis was performed in R primarily using the package "limma". 
The manuscript describing the methodology and results is being submitted to Genes, Brains, and Behaviors in the summer of 2020. 
"microRNA regulation related to the protective effects of environmental enrichment against cocaine-seeking behavior". 
Authors: Annika Vannan, Gregory L. Powell, Michela Dell’Orco, Melissa A. Wilson, Nora I. Perrone-Bizzozero, Janet L. Neisewander. 